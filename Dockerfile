﻿FROM mcr.microsoft.com/dotnet/aspnet:6.0 AS base
WORKDIR /app
EXPOSE 80
ENV LANG="de_DE.UTF-8"
ENV LANGUAGE="de_DE:de"
ENV LC_ALL="de_DE.UTF-8"
RUN addgroup --gid 2000 aspnetcore && \
    adduser --uid 2000 --ingroup aspnetcore --no-create-home --disabled-login --shell /bin/bash aspnetcore

FROM mcr.microsoft.com/dotnet/sdk:6.0 AS restore
WORKDIR /src
COPY src/QuotationWebCalculator.csproj .
RUN dotnet restore

FROM restore AS migration
COPY default-data.sql .
COPY src/ .
RUN apt update && apt install -y sqlite3
RUN dotnet tool install --global dotnet-ef
RUN export PATH="$PATH:/root/.dotnet/tools" && \
    dotnet ef migrations add InitialCreate && \
    dotnet ef database update
RUN cat default-data.sql | sqlite3 QuotationWebCalculator.sqlite3

FROM migration AS publish
RUN dotnet publish -c Release -o /app/publish

FROM base AS final
WORKDIR /app
COPY --from=publish /app/publish .
COPY --from=migration /src/QuotationWebCalculator.sqlite3 .
RUN chown -R aspnetcore:aspnetcore .
USER 2000
ENTRYPOINT ["dotnet", "QuotationWebCalculator.dll"]