#!/bin/bash
set -e

IMAGE_TAG="1.0"
IMAGE_NAME="quotationwebcalculator:${IMAGE_TAG}"
CONTAINER_NAME="webcalculator"

docker build . -t $IMAGE_NAME

if docker ps -a | grep $CONTAINER_NAME > /dev/null; then
    docker rm -f $CONTAINER_NAME
fi

docker run -d --name $CONTAINER_NAME -p 8080:80 $IMAGE_NAME