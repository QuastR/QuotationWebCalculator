# QuotationWebCalculator

FOM student project

## Run Application

The following describes how the application can be started as a Docker-Container.

### Run with script

1. Check if Docker is installed on your system
2. Clone this git Repository
3. Change to the cloned directory on the command line
4. Build and run Container
    * Linux (Bash is used): `./run-docker.sh`

### Build Dockerimage manual

1. Check if Docker is installed on your system
2. Clone this git Repository
3. Change to the cloned directory on the command line
4. Build Dockerimage: `docker build . -t quotationwebcalculator:1.0`
5. Run Container: `docker run -d --name webcalculator -p 8080:80 quotationwebcalculator:1.0`

## Accessing the application

* Open the URL `http://{HOSTNAME}:8080` in a web browser

## Application via https

To make the application callable via https, a reverse proxy can be used. (Example: nginx or traefik)
Alternatively, the application can also be equipped directly with a certificate. Adaptations in the source code are necessary for this.

## SQLite Database access

To access the SQLite database file from the host system, you can either use a Docker volume or copy the file from the container to the host with `docker cp`.
