using System.ComponentModel.DataAnnotations;

namespace QuotationWebCalculator.Models;

public class DurationUnit
{
    [Key]
    public int PK_DurationUnit_ID { get; set; }
    public string DurationName { get; set; }
}