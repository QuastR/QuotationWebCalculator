using System.ComponentModel.DataAnnotations;

namespace QuotationWebCalculator.Models;

public class ServicePackage
{
    [Key]
    public int PK_ServicePackage_ID { get; set; }
    public string Designation { get; set; }
}