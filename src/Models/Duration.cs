using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace QuotationWebCalculator.Models;

public class Duration
{
    [Key]
    public int PK_Duration_ID { get; set; }
    public int DurationInUnit { get; set; }

    [ForeignKey("DurationUnit")]
    public int FK_DurationUnit_ID { get; set; }

    // Navigation Property for Foreign Key
    public DurationUnit DurationUnit { get; set; }
}