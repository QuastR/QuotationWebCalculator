using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace QuotationWebCalculator.Models;

public class StaggeredPrice
{
    [ForeignKey("ServicePackage")]
    public int FK_ServicePackage_ID { get; set; }
    public int Quantity { get; set; }

    [ForeignKey("Duration")]
    public int FK_Duration_ID { get; set; }
    public double PriceInEuro { get; set; }
    public double PatchManagementPriceInEuro { get; set; }

    // Navigation Property for Foreign Key
    public ServicePackage ServicePackage { get; set; }
    public Duration Duration { get; set; }
}