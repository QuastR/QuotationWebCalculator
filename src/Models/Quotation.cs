using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace QuotationWebCalculator.Models;

public class Quotation
{
    // Company data
    [Key]
    public int PK_Quotation_ID { get; set; }

    [Required]
    public string CompanyName { get; set; }

    [Required]
    public string Street { get; set; }

    [Required]
    public int Postcode { get; set; }

    [Required]
    public string Location { get; set; }

    // Contact
    public string FirstName { get; set; }
    public string LastName { get; set; }

    [Required]
    public string Email { get; set; }
    public string TelephoneNumber { get; set; }

    // Date Quotation
    [Required]
    public DateTime DateQuotation { get; set; }

    // Quotation content
    [ForeignKey("ServicePackage")]
    public int FK_ServicePackage_ID { get; set; }

    [ForeignKey("Duration")]
    public int FK_Duration_ID { get; set; }
    
    public bool PatchManagement { get; set; }
    
    [Required]
    public int NumberOfUnits { get; set; }
    public string Info { get; set; }

    // When was the quotation created, when was it changed
    public DateTime DateTimeCreation { get; set; }
    public DateTime DateTimeModification { get; set; }

    // Navigation Property for Foreign Key
    public ServicePackage ServicePackage { get; set; }
    public Duration Duration { get; set; }
}