using Microsoft.AspNetCore.Mvc.Rendering;

namespace QuotationWebCalculator.Models;

public class QuotationCreateViewModel
{
    public Quotation Quotation { get; set; }
    public IEnumerable<SelectListItem> ServicePackages { get; set; }
    public IEnumerable<SelectListItem> Durations { get; set; }
}