namespace QuotationWebCalculator.Models;

public class PdfViewModel
{
    public Quotation Quotation { get; set; }
    public string NameOfServicePackage { get; set; }
    public string ServicePackageDuration { get; set; }
    public double MonthlyPriceInEuroServicePackage { get; set; }
    public double MonthlyPriceInEuroPatchManagement { get; set; }
    public double MonthlyPriceInEuroSummary { get; set; }
}