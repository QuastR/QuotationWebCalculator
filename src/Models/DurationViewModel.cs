namespace QuotationWebCalculator.Models;

public class DurationViewModel
{
    public Duration Duration { get; set; }
    public DurationUnit DurationUnit { get; set; }
}