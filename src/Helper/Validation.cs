using QuotationWebCalculator.Models;

namespace QuotationWebCalculator.Helper;

public class Validation
{
    private Quotation quotation;
    public Validation(Quotation _quotation)
    {
        quotation = _quotation;
    }

    public bool IsValid()
    {
        bool returnValue = false;

        bool filledCompanyName = false;
        bool filledStreet = false;
        bool filledPostcode = false;
        bool filledLocation = false;
        bool filledEmail = false;
        bool filledTelephoneNumber = false;
        bool filledFK_ServicePackage_ID = false;
        bool filledFK_Duration_ID = false;
        bool filledNumberOfUnits = false;

        if (quotation.CompanyName != null) { filledCompanyName = true; }
        if (quotation.Street != null) { filledStreet = true; }
        if (quotation.Postcode > 0) { filledPostcode = true; }
        if (quotation.Location != null) { filledLocation = true; }
        if (quotation.Email != null) { filledEmail = true; }
        if (quotation.FK_ServicePackage_ID > 0) { filledFK_ServicePackage_ID = true; }
        if (quotation.FK_Duration_ID  > 0) { filledFK_Duration_ID = true; }
        if (quotation.NumberOfUnits  > 0) { filledNumberOfUnits = true; }

        if (quotation.TelephoneNumber != null)
        {
            try
            {
                int convert = Convert.ToInt32(quotation.TelephoneNumber);
                filledTelephoneNumber = true;
            }
            catch (Exception)
            {
                Console.WriteLine("Invalid TelephoneNumber!");
            }
        }

        if (filledCompanyName && filledStreet && filledPostcode && filledLocation && filledEmail && filledFK_ServicePackage_ID && filledFK_Duration_ID && filledNumberOfUnits && filledTelephoneNumber)
        {
            returnValue = true;
        }

        return returnValue;
    }
}