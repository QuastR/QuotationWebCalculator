using iTextSharp.text;
using iTextSharp.text.pdf;
using iTextSharp.text.html.simpleparser;

namespace QuotationWebCalculator.Helper;

public class PdfRenderer
{
    private string _html;
    public PdfRenderer(string html)
    {
        _html = html;
    }

    public byte[] Render()
    {
        StringReader stringReader = new StringReader(_html);

        Document document = new Document();
        document.SetPageSize(PageSize.A4);
        document.SetMargins(20f, 20f, 20f, 20f);

        MemoryStream memoryStream = new MemoryStream();
        PdfWriter pdfWriter = PdfWriter.GetInstance(document, memoryStream);
        HtmlWorker htmlWorker = new HtmlWorker(document);
        
        document.Open();
        htmlWorker.StartDocument();
        htmlWorker.Parse(stringReader);
        htmlWorker.EndDocument();
        htmlWorker.Close();
        document.Close();

        byte[] pdfContent = memoryStream.ToArray();

        return pdfContent;
    }

    public void Save(string _filePath, byte[] _data)
    {
        File.WriteAllBytes(_filePath, _data);
    }
}