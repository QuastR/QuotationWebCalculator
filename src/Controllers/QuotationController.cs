﻿using System.Diagnostics;
using Microsoft.AspNetCore.Mvc;
using QuotationWebCalculator.Models;
using QuotationWebCalculator.Data;
using QuotationWebCalculator.Helper;
using Microsoft.AspNetCore.Mvc.Rendering;

namespace QuotationWebCalculator.Controllers;

public class QuotationController : Controller
{
    private readonly ILogger<QuotationController> _logger;
    private readonly ApplicationDbContext _dbContext;
    private readonly string activeNavCssClass = "active";

    public QuotationController(ILogger<QuotationController> logger, ApplicationDbContext dbContext)
    {
        _logger = logger;
        _dbContext = dbContext;
    }

    // Create Quotation page
    public IActionResult Create(int id)
    {
        Quotation quotation = null;

        List<ServicePackage> servicePackages = _dbContext.ServicePackages.ToList();
        List<Duration> durations = _dbContext.Durations.ToList();
        List<DurationUnit> listDurationUnits = _dbContext.DurationUnits.ToList();

        List<SelectListItem> listItemsServicePackages = new List<SelectListItem>();
        List<SelectListItem> listItemsDurations = new List<SelectListItem>();

        foreach (ServicePackage servicePackage in servicePackages)
        {
            listItemsServicePackages.Add(new SelectListItem() { Value = servicePackage.PK_ServicePackage_ID.ToString(), Text = servicePackage.Designation });
        }

        IEnumerable<DurationViewModel> durationViewModels = from d in durations join du in listDurationUnits on d.FK_DurationUnit_ID equals du.PK_DurationUnit_ID select new DurationViewModel { Duration = d, DurationUnit = du }; 

        foreach (DurationViewModel item in durationViewModels)
        {
            listItemsDurations.Add(new SelectListItem() { Value = item.Duration.PK_Duration_ID.ToString(), Text = item.Duration.DurationInUnit.ToString() + " " + item.DurationUnit.DurationName });
        }

        if (id > 0)
        {
            quotation = _dbContext.Quotations.Find(id);

            if (quotation != null)
            {
                ViewData["BtnText"] = "Änderung speichern";
            }
            else
            {
                quotation.PK_Quotation_ID = 0;
            }
        }

        QuotationCreateViewModel viewModel = new QuotationCreateViewModel() { Quotation = quotation, ServicePackages = listItemsServicePackages, Durations = listItemsDurations };

        ViewData["ActivePageCreate"] = activeNavCssClass;
        return View(viewModel);
    }

    [HttpPost]
    public IActionResult CreateEditQuotation(QuotationCreateViewModel quotationCreateViewModel)
    {
        if (new Validation(quotationCreateViewModel.Quotation).IsValid())
        {
            if (quotationCreateViewModel.Quotation.DateQuotation.ToShortDateString() == "01.01.0001")
            {
                quotationCreateViewModel.Quotation.DateQuotation = DateTime.Now;
            }

            if (quotationCreateViewModel.Quotation.Info == null)
            {
                quotationCreateViewModel.Quotation.Info = "";
            }

            if (quotationCreateViewModel.Quotation.PK_Quotation_ID == 0)
            {
                quotationCreateViewModel.Quotation.DateTimeCreation = DateTime.Now;
                _dbContext.Quotations.Add(quotationCreateViewModel.Quotation);
            }
            else
            {
                _dbContext.Quotations.Update(quotationCreateViewModel.Quotation);
            }

            quotationCreateViewModel.Quotation.DateTimeModification = DateTime.Now;

            _dbContext.SaveChanges();
            return RedirectToAction("Created");
        }
        else
        {
            TempData["AlertData"] = "Ungültige Eingabe! Bitte alle erforderlichen Felder ausfüllen sowie Leistungspaket und Laufzeit wählen";

            if (quotationCreateViewModel.Quotation.PK_Quotation_ID > 0)
            {
                return RedirectToAction("Create", new { Id = quotationCreateViewModel.Quotation.PK_Quotation_ID });
            }
            else
            {
                return RedirectToAction("Create");
            }
        }
    }

    public IActionResult Delete(int id)
    {
        if (id > 0)
        {
            Quotation quotationToDelete = _dbContext.Quotations.Find(id);

            if (quotationToDelete != null)
            {
                _dbContext.Quotations.Remove(quotationToDelete);
                _dbContext.SaveChanges();
            }
        }

        return RedirectToAction("Created");
    }

    // Created Quotation page
    public IActionResult Created()
    {
        var createdQuotation = _dbContext.Quotations.ToList();
        ViewBag.CreatedQuotations = createdQuotation;
        ViewData["ActivePageCreated"] = activeNavCssClass;
        return View();
    }

    // PDF
    public IActionResult CreatePdf(int id)
    {
        if (id > 0)
        {
            Quotation quotation = _dbContext.Quotations.Find(id);

            if (quotation != null)
            {
                var queryDuration = from q in _dbContext.Quotations join d in _dbContext.Durations on q.FK_Duration_ID equals d.PK_Duration_ID join dunit in _dbContext.DurationUnits on d.FK_DurationUnit_ID equals dunit.PK_DurationUnit_ID where q.PK_Quotation_ID == quotation.PK_Quotation_ID select new { d.DurationInUnit, dunit.DurationName };
                var entryDuration = queryDuration.First();

                var queryPrices = from sp in _dbContext.StaggeredPrices where sp.FK_ServicePackage_ID == quotation.FK_ServicePackage_ID && sp.FK_Duration_ID == quotation.FK_Duration_ID && sp.Quantity <= quotation.NumberOfUnits orderby sp.Quantity descending select new { sp.PriceInEuro, sp.PatchManagementPriceInEuro };
                var entryPrices = queryPrices.First();

                double patchManagementPrice = 0d;
                string patchManagementYesNo = "Keine";

                if (quotation.PatchManagement)
                {
                    patchManagementPrice = entryPrices.PatchManagementPriceInEuro;
                    patchManagementYesNo = "Patch Managemet für " + patchManagementPrice + " € pro Gerät";
                }

                PdfViewModel viewModel = new PdfViewModel();
                viewModel.Quotation = quotation;
                viewModel.NameOfServicePackage = _dbContext.ServicePackages.Find(quotation.FK_ServicePackage_ID).Designation;
                viewModel.ServicePackageDuration = entryDuration.DurationInUnit + " " + entryDuration.DurationName;
                viewModel.MonthlyPriceInEuroServicePackage = entryPrices.PriceInEuro;
                viewModel.MonthlyPriceInEuroPatchManagement = patchManagementPrice;
                viewModel.MonthlyPriceInEuroSummary = (entryPrices.PriceInEuro + patchManagementPrice) * quotation.NumberOfUnits;

                string html = "<!DOCTYPE html><html lang='de'><head><meta charset='utf-8' /></head><body><h1>CLM-Angebot vom " + viewModel.Quotation.DateQuotation.ToShortDateString() + "</h1><br><br><p>" + viewModel.Quotation.CompanyName + "<br>" + viewModel.Quotation.Street + "<br>" + viewModel.Quotation.Postcode + "<br>" + viewModel.Quotation.Location + "</p><br><p>" + viewModel.Quotation.LastName + "<br>" + viewModel.Quotation.FirstName + "<br>" + viewModel.Quotation.Email + "<br>" + viewModel.Quotation.TelephoneNumber + "</p><br><br><h2>Leistungspaket </h2><p>" + viewModel.NameOfServicePackage + " für " + viewModel.MonthlyPriceInEuroServicePackage + " € pro Gerät</p><br><h2>Zusatzleistungen</h2><p>" + patchManagementYesNo + "</p><br><h2>Anzahl der Geräte</h2><p>Anzahl der Geräte: " + viewModel.Quotation.NumberOfUnits + "</p><br><h2>Laufzeit</h2><p>Die oben aufgeführten Leistungen sind für " + viewModel.ServicePackageDuration + " gebucht.</p><br><h2>Monatlicher Gesamtpreis</h2><p>Der monatliche Gesamtpreis für die Client-Infrastruktur beträgt " + viewModel.MonthlyPriceInEuroSummary + " €.</h3><br><h2>Zusätzliche Informationen</h2><p>" + viewModel.Quotation.Info + "</p></body></html>";
                PdfRenderer renderer = new PdfRenderer(html);
                byte[] fileByteArray = renderer.Render();

                return File(fileByteArray, "application/pdf", "Angebot.pdf");
            }
        }

        return RedirectToAction("Created");
    }

    [ResponseCache(Duration = 0, Location = ResponseCacheLocation.None, NoStore = true)]
    public IActionResult Error()
    {
        return View(new ErrorViewModel { RequestId = Activity.Current?.Id ?? HttpContext.TraceIdentifier });
    }
}
