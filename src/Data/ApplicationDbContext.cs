using Microsoft.EntityFrameworkCore;
using QuotationWebCalculator.Models;

namespace QuotationWebCalculator.Data;

public class ApplicationDbContext : DbContext
{
    public ApplicationDbContext(DbContextOptions<ApplicationDbContext> options) : base(options)
    {

    }

    protected override void OnModelCreating(ModelBuilder modelBuilder)
    {
        modelBuilder.Entity<StaggeredPrice>().HasKey(pk => new { pk.FK_ServicePackage_ID, pk.Quantity, pk.FK_Duration_ID });
    }

    public DbSet<ServicePackage> ServicePackages { get; set; }
    public DbSet<Duration> Durations { get; set; }
    public DbSet<DurationUnit> DurationUnits { get; set; }
    public DbSet<Quotation> Quotations { get; set; }
    public DbSet<StaggeredPrice> StaggeredPrices { get; set; }
}