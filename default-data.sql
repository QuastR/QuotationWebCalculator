INSERT INTO ServicePackages (Designation) VALUES ("Leistungspaket 1");
INSERT INTO ServicePackages (Designation) VALUES ("Leistungspaket 2");
INSERT INTO ServicePackages (Designation) VALUES ("Leistungspaket 3");

INSERT INTO DurationUnits (DurationName) VALUES ("Jahre");

INSERT INTO Durations (DurationInUnit, FK_DurationUnit_ID) VALUES (3, 1);
INSERT INTO Durations (DurationInUnit, FK_DurationUnit_ID) VALUES (4, 1);

INSERT INTO StaggeredPrices (FK_ServicePackage_ID, Quantity, FK_Duration_ID, PriceInEuro, PatchManagementPriceInEuro) VALUES (1, 10, 1, 6.54, 4.08);
INSERT INTO StaggeredPrices (FK_ServicePackage_ID, Quantity, FK_Duration_ID, PriceInEuro, PatchManagementPriceInEuro) VALUES (1, 20, 1, 3.54, 3.3);
INSERT INTO StaggeredPrices (FK_ServicePackage_ID, Quantity, FK_Duration_ID, PriceInEuro, PatchManagementPriceInEuro) VALUES (1, 50, 1, 2.19, 2.82);
INSERT INTO StaggeredPrices (FK_ServicePackage_ID, Quantity, FK_Duration_ID, PriceInEuro, PatchManagementPriceInEuro) VALUES (1, 100, 1, 1.5, 2.34);

INSERT INTO StaggeredPrices (FK_ServicePackage_ID, Quantity, FK_Duration_ID, PriceInEuro, PatchManagementPriceInEuro) VALUES (1, 10, 2, 5.28, 4.08);
INSERT INTO StaggeredPrices (FK_ServicePackage_ID, Quantity, FK_Duration_ID, PriceInEuro, PatchManagementPriceInEuro) VALUES (1, 20, 2, 2.94, 3.3);
INSERT INTO StaggeredPrices (FK_ServicePackage_ID, Quantity, FK_Duration_ID, PriceInEuro, PatchManagementPriceInEuro) VALUES (1, 50, 2, 1.86, 2.82);
INSERT INTO StaggeredPrices (FK_ServicePackage_ID, Quantity, FK_Duration_ID, PriceInEuro, PatchManagementPriceInEuro) VALUES (1, 100, 2, 1.32, 2.34);

INSERT INTO StaggeredPrices (FK_ServicePackage_ID, Quantity, FK_Duration_ID, PriceInEuro, PatchManagementPriceInEuro) VALUES (2, 10, 1, 8.87, 4.08);
INSERT INTO StaggeredPrices (FK_ServicePackage_ID, Quantity, FK_Duration_ID, PriceInEuro, PatchManagementPriceInEuro) VALUES (2, 20, 1, 5.88, 3.3);
INSERT INTO StaggeredPrices (FK_ServicePackage_ID, Quantity, FK_Duration_ID, PriceInEuro, PatchManagementPriceInEuro) VALUES (2, 50, 1, 4.5, 2.82);
INSERT INTO StaggeredPrices (FK_ServicePackage_ID, Quantity, FK_Duration_ID, PriceInEuro, PatchManagementPriceInEuro) VALUES (2, 100, 1, 3.9, 2.34);

INSERT INTO StaggeredPrices (FK_ServicePackage_ID, Quantity, FK_Duration_ID, PriceInEuro, PatchManagementPriceInEuro) VALUES (2, 10, 2, 7.02, 4.08);
INSERT INTO StaggeredPrices (FK_ServicePackage_ID, Quantity, FK_Duration_ID, PriceInEuro, PatchManagementPriceInEuro) VALUES (2, 20, 2, 4.68, 3.3);
INSERT INTO StaggeredPrices (FK_ServicePackage_ID, Quantity, FK_Duration_ID, PriceInEuro, PatchManagementPriceInEuro) VALUES (2, 50, 2, 3.66, 2.82);
INSERT INTO StaggeredPrices (FK_ServicePackage_ID, Quantity, FK_Duration_ID, PriceInEuro, PatchManagementPriceInEuro) VALUES (2, 100, 2, 3.12, 2.34);

INSERT INTO StaggeredPrices (FK_ServicePackage_ID, Quantity, FK_Duration_ID, PriceInEuro, PatchManagementPriceInEuro) VALUES (3, 10, 1, 11.76, 4.08);
INSERT INTO StaggeredPrices (FK_ServicePackage_ID, Quantity, FK_Duration_ID, PriceInEuro, PatchManagementPriceInEuro) VALUES (3, 20, 1, 7.02, 3.3);
INSERT INTO StaggeredPrices (FK_ServicePackage_ID, Quantity, FK_Duration_ID, PriceInEuro, PatchManagementPriceInEuro) VALUES (3, 50, 1, 5.7, 2.82);
INSERT INTO StaggeredPrices (FK_ServicePackage_ID, Quantity, FK_Duration_ID, PriceInEuro, PatchManagementPriceInEuro) VALUES (3, 100, 1, 5.04, 2.34);

INSERT INTO StaggeredPrices (FK_ServicePackage_ID, Quantity, FK_Duration_ID, PriceInEuro, PatchManagementPriceInEuro) VALUES (3, 10, 2, 7.92, 4.08);
INSERT INTO StaggeredPrices (FK_ServicePackage_ID, Quantity, FK_Duration_ID, PriceInEuro, PatchManagementPriceInEuro) VALUES (3, 20, 2, 5.58, 3.3);
INSERT INTO StaggeredPrices (FK_ServicePackage_ID, Quantity, FK_Duration_ID, PriceInEuro, PatchManagementPriceInEuro) VALUES (3, 50, 2, 4.5, 2.82);
INSERT INTO StaggeredPrices (FK_ServicePackage_ID, Quantity, FK_Duration_ID, PriceInEuro, PatchManagementPriceInEuro) VALUES (3, 100, 2, 3.96, 2.34);